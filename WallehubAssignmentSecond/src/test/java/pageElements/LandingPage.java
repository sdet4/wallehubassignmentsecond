package pageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import objecthandler.ObjectHandler;

public class LandingPage {
	@FindBy(xpath="//span[text()='Login']")
	WebElement loginBtn;
	
//	Initializing page objects
	public LandingPage() {
		try {
			PageFactory.initElements(ObjectHandler.getObjectHandlerInstance().getDriver(), this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickOnLogin() {
		loginBtn.click();
	}

}
