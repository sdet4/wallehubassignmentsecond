package pageElements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import containers.DriverHandler;
import objecthandler.ObjectHandler;

public class ReviewPage extends DriverHandler{
	@FindBy(xpath="//h4[@text()='Test Insurance Company']")
	WebElement txt_header;
	@FindBy(xpath="//textarea[@placeholder='Write your review...']")
	WebElement txtbox_review;
	@FindBy(xpath="//div[@class='dropdown second']")
	WebElement dropdown_icon;
	@FindBy(xpath="//div[@text()='Submit']")
	WebElement btn_Submit;

	WebDriver driver;
	
//	Initializing page objects
	public ReviewPage() {
		try {
			driver = ObjectHandler.getObjectHandlerInstance().getDriver();
			PageFactory.initElements(driver, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writeReview(String message) {
		txtbox_review.sendKeys(message);		
	}

	public void setDropdownValue(String text) {
		dropdown_icon.click();
		WebElement dropdown_option = driver.findElement(By.cssSelector("li[@text()='" + text + "']"));
		dropdown_option.click();
		Assert.assertTrue(dropdown_option.getAttribute("class").contains("active"), "dropdown value "+text+" is not selected ");
	}

	public void submit() {
		btn_Submit.click();		
	}

	public void verifySuccessMessage(String message) {
		Assert.assertTrue(driver.findElement(By.xpath("//h4[[@text()='"+message+"']")).isDisplayed(), "Success message is not displayed. ");		
	}

}
