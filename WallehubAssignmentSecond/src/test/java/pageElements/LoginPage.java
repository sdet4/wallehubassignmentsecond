package pageElements;

import java.util.Base64;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import configs.EnvironmentalDetails;
import containers.DriverHandler;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import objecthandler.ObjectHandler;

public class LoginPage extends EnvironmentalDetails{
	WebDriver driver;
	@FindBy(id="email")
	WebElement txtbox_username;
	@FindBy(id="password")
	WebElement txtbox_password;
	@FindBy(xpath="//span[text()='Login']")
	WebElement btnSubmit;
	
	public LoginPage() {
		try {
			driver = ObjectHandler.getObjectHandlerInstance().getDriver();
			PageFactory.initElements(driver, this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public void Login() {
		byte[] decodedBytes = Base64.getDecoder().decode(properties.getProperty("password"));
		String decodedString = new String(decodedBytes);
		txtbox_username.sendKeys(properties.getProperty("username"));
		txtbox_password.sendKeys(decodedString);
		btnSubmit.click();
	}
	
	public String getPageTitle() {
		return driver.getTitle();
	}
	@Before 
	public void setUp(){    
		EnvironmentalDetails environmentalDetails = new EnvironmentalDetails();
		try {
			createDriver();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	   }  
	
	@After 
	public void cleanUp(){    
	      driver.close();    
	   }  
}
