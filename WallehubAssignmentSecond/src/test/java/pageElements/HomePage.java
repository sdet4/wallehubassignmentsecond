package pageElements;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import containers.DriverHandler;
import objecthandler.ObjectHandler;

public class HomePage extends DriverHandler{
	
	@FindBy(xpath="//span[@text()='Login']")
	WebElement loginBtn;
	@FindBy(xpath="//h3/parent::div//review-star/div/*[name()='svg']/*[name()='g']/*[name()='path' and @fill='#4ae0e1']")
	List<WebElement> icon_rating_Stars;
	@FindBy(xpath="//h3/parent::div//review-star/div/*[name()='svg']/*[name()='g']/*[name()='path' and @fill='none']")
	List<WebElement> icon_rating_Stars_Selected;
	@FindBy(xpath="//*[@id='reviews-section']/section/article[1]/div[4]")
	WebElement txt_ReviewMessage;
	
//	Initializing page objects
	public HomePage() {
		try {
			PageFactory.initElements(ObjectHandler.getObjectHandlerInstance().getDriver(), this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setRatingStars(int stars) {
		Actions action = null;
		try {
			action = new Actions(ObjectHandler.getObjectHandlerInstance().getDriver());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		action.moveToElement(icon_rating_Stars.get(stars-1)).perform();
		Assert.assertSame(icon_rating_Stars_Selected.size()+1, stars, "number of stars lit up are incorrect ");
		icon_rating_Stars.get(stars-1).click();		
	}

	public void verifyReviewMessage(String message) {
		Assert.assertSame(txt_ReviewMessage.getText(), message, "Review message is not correct ");
	}

	public void verifyPageTitle(String title) {
		WebDriverWait wait=new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.titleIs(title));
	}

}
