package pageElements;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import containers.DriverHandler;
import objecthandler.ObjectHandler;

public class ProfilePage extends DriverHandler{
	@FindBy(xpath="//h2/parent::section/div/div//review-star/div/*[name()='svg']/*[name()='g']/*[name()='path' and @fill='none']")
	List<WebElement> icon_rating_Stars_Selected;
	@FindBy(xpath="//h2/parent::section/div/div//review-star/parent::div/a")
	WebElement lnk_Review;
	
//	Initializing page objects
	public ProfilePage() {
		try {
			PageFactory.initElements(ObjectHandler.getObjectHandlerInstance().getDriver(), this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickOnReview() {
		lnk_Review.click();
	}

	public void verifyReviewIsDisplayed(int stars) {
		Assert.assertSame(icon_rating_Stars_Selected.size()+1, stars, "number of stars lit up are incorrect ");
	}

}
