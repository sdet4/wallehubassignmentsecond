package pageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import containers.DriverHandler;
import objecthandler.ObjectHandler;

public class HeaderPage extends DriverHandler{
	@FindBy(xpath="//nav[@class='burger-menu-right-menu']/a[@aria-label='My Wallet']")
	WebElement lnk_MyWallet;
	@FindBy(xpath="//nav[@class='burger-menu-right-menu']/a[@aria-label='Credit Cards']")
	WebElement lnk_CreditCards;
	@FindBy(xpath="//nav[@class='burger-menu-right-menu']/div[@class='brgm-button brgm-user brgm-list-box/span']")
	WebElement lnk_User;
	@FindBy(xpath="//nav[@class='burger-menu-right-menu']/div[@class='brgm-button brgm-user brgm-list-box/div']")
	WebElement lnk_UserProfile;
	
//	Initializing page objects
	public HeaderPage() {
		try {
			PageFactory.initElements(ObjectHandler.getObjectHandlerInstance().getDriver(), this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickOnUser() {
		lnk_User.click();
	}

	public void navigateToProfile() {
		Actions action = null;
		try {
			action = new Actions(ObjectHandler.getObjectHandlerInstance().getDriver());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		action.moveToElement(lnk_User).perform();
		lnk_UserProfile.click();
	}

}
