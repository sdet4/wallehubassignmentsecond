package objecthandler;

import containers.DriverHandler;
import containers.FileHandler;
import pageElements.HeaderPage;
import pageElements.HomePage;
import pageElements.LandingPage;
import pageElements.LoginPage;
import pageElements.ProfilePage;
import pageElements.ReviewPage;

public class ObjectHandler extends DriverHandler {
	static ObjectHandler objectHandler;
	static LandingPage landingPage;
	static HomePage homePage;
	static LoginPage loginPage;
	static ReviewPage reviewPage;
	static HeaderPage headerPage;
	static ProfilePage profilePage;
	
	public ObjectHandler() {
		// TODO Auto-generated constructor stub
	}
	public static ObjectHandler getObjectHandlerInstance() {
		if(objectHandler == null) {
			objectHandler = new ObjectHandler();
		}
		return objectHandler;
	}
	public static LandingPage getLandingPageInstance() {
		if(landingPage == null) {
			landingPage = new LandingPage();
		}
		return landingPage;
	}
	public static HomePage getHomePageInstance() {
		if(homePage == null) {
			homePage = new HomePage();
		}
		return homePage;
	}
	public static LoginPage getLoginPageInstance() {
		if(loginPage == null) {
			loginPage = new LoginPage();
		}
		return loginPage;
	}
	public static ReviewPage getReviewPageInstance() {
		if(reviewPage == null) {
			reviewPage = new ReviewPage();
		}
		return reviewPage;
	}
	public static HeaderPage getHeaderPageInstance() {
		if(headerPage == null) {
			headerPage = new HeaderPage();
		}
		return headerPage;	
		}
	public static ProfilePage getProfilePageInstance() {
		if(profilePage == null) {
			profilePage = new ProfilePage();
		}
		return profilePage;	
		}
}
