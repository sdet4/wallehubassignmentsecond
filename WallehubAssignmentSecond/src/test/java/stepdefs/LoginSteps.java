package stepdefs;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import objecthandler.ObjectHandler;

public class LoginSteps {
	String message = "Hi, This is a test review, Please ignore this review as this is test review....."
			+ "This review contains at least 132 characters";

	@Given("user is on login page")
	public void user_is_on_login_page() {

		System.out.println("Hello user is on login page");
		try {
			//open app url
			ObjectHandler.getObjectHandlerInstance().getDriver().get("http://wallethub.com/profile/test_insurance_company/");
			//click on login
			ObjectHandler.getLandingPageInstance().clickOnLogin();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@When("user enters username and password and click on Login")
	public void user_enters_username_and_password_and_click_on_login() {
	
		System.out.println("Hello user enters username and password");
		//enter email address
		//enter password
		//click on login btn
		ObjectHandler.getLoginPageInstance().Login();
		
	}

	@Then("user is navigated to the homepage")
	public void user_is_navigated_to_the_homepage() {
		System.out.println("Hello user is navigated to the homepage");
		ObjectHandler.getHomePageInstance().verifyPageTitle("test insurance company metatitle test");
	}
	
	@When("user gives the rating as {int} and adds the review message")
	public void user_gives_the_rating_as_and_adds_the_review_message(Integer stars) {
		//mousehover on 4th star and verify that the start is highlighted
		//click on 4th star
		//redirected to write a review page
		//verify 4 stars are selected
		ObjectHandler.getHomePageInstance().setRatingStars(stars);
		
		//click on write a review textarea
		//write random texts 200 chars
		ObjectHandler.getReviewPageInstance().writeReview(message);

	}

	@When("Select the category {string} and click on Submit")
	public void select_the_category_and_click_on_submit(String string) {
		//click on dropdown 
				//select health insurance
				ObjectHandler.getReviewPageInstance().setDropdownValue("Health Insurance");
				
				//click submit
				ObjectHandler.getReviewPageInstance().submit();

	}

	@Then("user is able to see the success message on the review page")
	public void user_is_able_to_see_the_success_message_on_the_review_page() {
		//verify success message 
		ObjectHandler.getReviewPageInstance().verifySuccessMessage("Your review has been posted.");
		
	}

	@When("user navigates to profile")
	public void user_navigates_to_profile() {
		//go to profile
		ObjectHandler.getHeaderPageInstance().clickOnUser();
		ObjectHandler.getHeaderPageInstance().navigateToProfile();
		
	}

	@Then("user is able to see the rating added")
	public void user_is_able_to_see_the_rating_added() {
		ObjectHandler.getProfilePageInstance().verifyReviewIsDisplayed(4);
	}

	@When("user clicks on the rating section")
	public void user_clicks_on_the_rating_section() {
		ObjectHandler.getProfilePageInstance().clickOnReview();

	}

	@Then("user is able to see the rating and review message in the feed")
	public void user_is_able_to_see_the_rating_and_review_message_in_the_feed() {
		//verify the review is added
		ObjectHandler.getHomePageInstance().verifyReviewMessage(message);
	}
	

}
