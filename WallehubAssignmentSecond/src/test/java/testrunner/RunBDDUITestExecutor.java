package testrunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions( features = "src/test/resources/Features",
					glue = "src/test/java/stepdefs",
					monochrome = true,
					plugin = {"pretty",
							"html:target/HtmlReports/report.html", 
							"json:target/JSONReports/Jsonreport.json", 
							"junit:target/JunitReports/JunitReport.xml"},
					tags = "@SmokeTests and @RegressionTests and UISprintTests and @GoLive and ~@Ignore"
				)

public class RunBDDUITestExecutor extends AbstractTestNGCucumberTests{

}
