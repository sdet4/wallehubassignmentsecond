package configs;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import containers.DriverHandler;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class EnvironmentalDetails extends DriverHandler{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentalDetails.class);
	public static String projectName = null;
    BufferedReader reader;

    public EnvironmentalDetails() {
    	super();
    	String propertyFilePath = getClass().getClassLoader().getResource("configs").getPath();
    	final String env = getEnvAsParam();
    	final String projectName = getProjectName();
    	try {
    	Map<String, String> envSelection = new HashMap<String,String>();

    	envSelection.put("QA", "/ui-bdd-qa.properties");
    	envSelection.put("DEV", "/ui-bdd-dev.properties");
    	envSelection.put("PROD", "/ui-bdd-prod.properties");
    	envSelection.put("LOCAL", "/ui-bdd-local.properties");
    	
    	InputStream input = new FileInputStream(propertyFilePath.concat(envSelection.get(env)));
    	properties.load(input);
    	}catch(FileNotFoundException e) {
    		e.printStackTrace();
    		throw new RuntimeException("property file not found at "+ propertyFilePath);
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }

    private String getProjectName() {
		// TODO Auto-generated method stub
		return null;
	}

	private HashMap<String, String> load(String filepath) throws IOException {
        HashMap<String, String> innerpropertyMap = new HashMap<String, String>();
        properties = new Properties();

        reader = new BufferedReader(new FileReader(filepath));
        properties.load(reader);
        reader.close();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            innerpropertyMap.put(key, value);
        }
        return innerpropertyMap;
    }


    private String getEnvAsParam() {
        if(System.getProperty("env")==null)
            System.setProperty("env","QA");
        return System.getProperty("env");
    }

    }