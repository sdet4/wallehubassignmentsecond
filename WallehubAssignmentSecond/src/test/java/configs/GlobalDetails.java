package configs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import utils.DriverType;
import utils.TestMachineType;

public class GlobalDetails {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDetails.class);
	private final String propertyFilePath = "/src/test/resources/configs/global.properties";
	public Properties properties;
	
	public GlobalDetails() {
		properties = new Properties();
		try {
			FileInputStream input = new FileInputStream(
					System.getProperty("user.dir") + propertyFilePath);
			properties.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("global.properties not found at "+ propertyFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}
	
	public String getDriverPath() {
		String browserName = System.getProperty("browser");
		File f = new File("");
		String driverPath = null;
		if(browserName == null || browserName.equalsIgnoreCase("chrome")) {
			driverPath = f.getAbsolutePath() + "\\src\\test\\resources\\driver\\chromedriver.exe";
		}
		else if(browserName == null || browserName.equalsIgnoreCase("IE")) {
			driverPath = f.getAbsolutePath() + "\\src\\test\\resources\\driver\\IEDriverserv.exe";
		}
		else if(browserName == null || browserName.equalsIgnoreCase("firefox")) {
			driverPath = f.getAbsolutePath() + "\\src\\test\\resources\\driver\\geckodriver.exe";
		}
		else {
			throw new RuntimeException("driver nor found for "+ browserName);
		}
		if(driverPath != null) {
			return driverPath;
		}
		else throw new RuntimeException(" Unable to recognize driverPath ");
	}

	public DriverType getBrowser() {
		// TODO Auto-generated method stub
		return DriverType.CHROME;
	}

	public TestMachineType getMachineType() {
		// TODO Auto-generated method stub
		return TestMachineType.LOCAL;
	}

	public boolean getBrowserWindowSize() {
		// TODO Auto-generated method stub
		return false;
	}

	public long getImplicitlyWait() {
		// TODO Auto-generated method stub
		return 30;
	}

}
