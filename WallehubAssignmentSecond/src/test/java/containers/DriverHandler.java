package containers;

import java.lang.System.Logger.Level;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import configs.GlobalDetails;
import configs.SauceLabDetails;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import utils.DriverType;
import utils.TestMachineType;

public class DriverHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDetails.class);
	public static WebDriver driver;
    public Properties     	properties = new Properties();
    public GlobalDetails globalDetails = new GlobalDetails();
	public SauceLabDetails sauceLabDetails = new SauceLabDetails();
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	private static DriverType driverType;
	private static TestMachineType machineType;
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String IE_DRIVER_PROPERTY = "webdriver.ie.driver";
	public static DesiredCapabilities capabilities;
	
	public DriverHandler() {
		driverType = globalDetails.getBrowser();
		machineType = globalDetails.getMachineType();
	}
	
	public WebDriver getDriver() throws Exception {
		if (driver == null) {
			driver = createDriver();
		}
		return driver;
	}
	
	protected WebDriver createDriver() throws Exception {
		switch(machineType) {
		case LOCAL:
			driver = createLocalDriver();
			break;
		case REMOTE:
			driver = createRemoteDriver();
			break;
		}
		return driver;
	}

	private WebDriver createRemoteDriver() throws Exception {
		LOGGER.info("Browser driver type is: "+ driverType);
		String sauceUserName = sauceLabDetails.getSauceLabProperty("sauceUserName");
		LOGGER.info("Sauce UserName is: "+sauceUserName);
		String sauceAccessKey = sauceLabDetails.getSauceLabProperty("sauceAccessKey");
		LOGGER.info("sauce Access Key is "+ sauceAccessKey);
		String tunnelID = sauceLabDetails.getSauceLabProperty("sauceTunnelID");
		LOGGER.info("tunnelID is "+ tunnelID);
		String clientHostName = sauceLabDetails.getSauceLabProperty("clientHostName");
		String portNumber = sauceLabDetails.getSauceLabProperty("proxyPort");
		String sauceURL = "http://"+sauceUserName+":"+tunnelID+"@"+clientHostName+"us.global.schwab.com:"
				+portNumber+"/wd/hub";
		LOGGER.info("sauceURL is "+ sauceURL);
		
		MutableCapabilities sauceOpts = new MutableCapabilities();
		sauceOpts.setCapability("username", sauceUserName);
		sauceOpts.setCapability("accessKey", sauceAccessKey);
		sauceOpts.setCapability("tunnelIdentifier", sauceLabDetails.getSauceLabProperty("sauceTunnel"));
		sauceOpts.setCapability("parentTunnel", sauceLabDetails.getSauceLabProperty("parentSauceTunnel"));
		sauceOpts.setCapability("seleniumVersion", sauceLabDetails.getSauceLabProperty("seleniumVer"));
		sauceOpts.setCapability("name", sauceLabDetails.getSauceLabProperty("testName"));
		List<String> tags = Arrays.asList("test","RegressionTests","SmokeTests","GoLive","UISPrintTests");
		sauceOpts.setCapability("tags", tags);
		sauceOpts.setCapability("maxDuration", 3600);
		sauceOpts.setCapability("commandTimeout", 600);
		sauceOpts.setCapability("idleTimeout", 1000);
		sauceOpts.setCapability("build", sauceLabDetails.getSauceLabProperty("buildName"));

		switch(driverType) {
			case FIREFOX:
				try {
					DesiredCapabilities mozillaCap = DesiredCapabilities.firefox();
					mozillaCap.setCapability("sauce:options", sauceOpts);
					mozillaCap.setBrowserName("firefox");
					mozillaCap.setPlatform(Platform.WINDOWS);
					mozillaCap.setVersion(sauceLabDetails.getSauceLabProperty("firefoxversion"));
					mozillaCap.setCapability("platform", sauceLabDetails.getSauceLabProperty("platform"));
					driver = new RemoteWebDriver(new URL(sauceURL), mozillaCap);
					driver.get(FileHandler.getInstance().getUIEvent().getAppUrl());
						
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				} catch (Exception fox) {
					throw new Exception("Unable to create an instance of "+ driverType + " webDriver");
				}
				break;
			case CHROME:
				try {
					ChromeOptions chromeOpts = new ChromeOptions();
					chromeOpts.setExperimentalOption("w3c", true);
					MutableCapabilities capabilities = new MutableCapabilities();
					capabilities.setCapability("sauce:options", sauceOpts);
					capabilities.setCapability("goog:chromeOptions", chromeOpts);
					capabilities.setCapability("browserName", "chrome");
					capabilities.setCapability("platform", sauceLabDetails.getSauceLabProperty("platform"));
					capabilities.setCapability("browserVersion", sauceLabDetails.getSauceLabProperty("chromeVersion"));
					driver = new RemoteWebDriver(new URL(sauceURL), capabilities);
					driver.get(FileHandler.getInstance().getUIEvent().getAppUrl());
				} catch (Exception e) {
					throw new Exception("Unable to create an instance of " + driverType+" webDriver ");
				}
				break;
			case INTERNETEXPLORER:
				try {
					InternetExplorerOptions options= new InternetExplorerOptions();
					options.enablePersistentHovering();
					options.introduceFlakinessByIgnoringSecurityDomains();
					options.ignoreZoomSettings();
					DesiredCapabilities iecapabilities = DesiredCapabilities.internetExplorer();
					iecapabilities.setCapability("browserName", "IE");
					iecapabilities.setCapability("browserVersion", "latest");
					iecapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					iecapabilities.setCapability("sauce:options", sauceOpts);
					iecapabilities.setBrowserName("internetExplorer");
					driver = new RemoteWebDriver(new URL(sauceURL), iecapabilities);
					driver.get(FileHandler.getInstance().getUIEvent().getAppUrl());
					LOGGER.info("IE Remote Instance created ");
				} catch (Exception e) {
					throw new Exception("Unable to create an instance of " + driverType+" webDriver ");
				}
				break;
		}
		if(globalDetails.getBrowserWindowSize()) {
			driver.manage().window().maximize();
		}
		driver.manage().timeouts().implicitlyWait(globalDetails.getImplicitlyWait(), TimeUnit.SECONDS);
		return driver;
	}

	private WebDriver createLocalDriver() {
		switch(driverType) {
		case FIREFOX:
			System.setProperty(FIREFOX_DRIVER_PROPERTY, globalDetails.getDriverPath());
			driver = new FirefoxDriver();
			break;
		case CHROME:
			System.setProperty(CHROME_DRIVER_PROPERTY, globalDetails.getDriverPath());
			driver = new ChromeDriver();
			break;
		case INTERNETEXPLORER:
			System.setProperty(IE_DRIVER_PROPERTY, globalDetails.getDriverPath());
			driver = new ChromeDriver();
			break;
		}

		
		return driver;
	}
	
}
