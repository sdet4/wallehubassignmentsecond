package containers;

import ui.factory.UIEvent;

public class FileHandler {
	static FileHandler fileHandler;
	static UIEvent uiEvent;
	
	public FileHandler() {
		// TODO Auto-generated constructor stub
	}

	public static FileHandler getInstance() {
		if(fileHandler == null) {
			fileHandler = new FileHandler();
		}
		return fileHandler;
	}

	public UIEvent getUIEvent() {
		if(uiEvent == null) {
			uiEvent = new UIEvent();
		}		return uiEvent;
	}

}
