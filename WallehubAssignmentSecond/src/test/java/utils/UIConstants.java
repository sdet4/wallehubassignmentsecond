package utils;

public class UIConstants {

	public static final String REGRESSION = "RegressionTests";
	public static final String SMOKE = "SmokeTests";
	public static final String GOLIVE = "GoLive";
	public static final String SPRINT = "Sprint";
	public static final String UISPRINT = "UISPrintTests";
	public static final String SUCCESS = "SUCCESS";
	public static final String BAD_REQUEST = "Bad Request";
	public static final String ERROR = "ERROR";
}
