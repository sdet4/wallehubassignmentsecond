#Author: shrikant007@hotmail.com


Feature: feature to test Review functionality

    Scenario: Check Review functionality 
    Given user is on login page
    When user enters username and password and click on Login
    Then user is navigated to the homepage
    When user gives the rating as 4 and adds the review message
    And Select the category "Health Insurance" and click on Submit 
    Then user is able to see the success message on the review page
    When user navigates to profile
    Then user is able to see the rating added
    When user clicks on the rating section
    Then user is able to see the rating and review message in the feed
    
    
    
    
    